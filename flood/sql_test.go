package flood_test

import (
	"context"
	"database/sql"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/fgmarand/gopal/flood"
)

const testDBPath = "test.db"

func testSQLSetup(t *testing.T) (ctx context.Context, flooder flood.Flooder) {
	ctx = context.Background()
	os.RemoveAll(testDBPath)
	db, err := sql.Open(`sqlite3`, testDBPath)
	if err != nil {
		t.Fatalf("could not open a SQLite test instance in %s: %v", testDBPath, err)
	}
	flooder = flood.NewSQL(ctx, db, "")
	return ctx, flooder
}

func TestSQL_Clear(t *testing.T) {
	checks := [...]struct {
		name            string
		event           flood.EventName
		id              flood.ID
		expectedCleared int
		expectedAllowed bool
	}{
		{"registered event+id", name, id, 1, true},
		{"unregistered event, registered id", name + name, id, 0, false},
		{"registered event, unregistered id", name, id + id, 0, false},
	}

	for _, check := range checks {
		check := check
		t.Run(check.name, func(t *testing.T) {
			ctx, flooder := testSQLSetup(t)
			defer flooder.Close()

			// Register expired event.
			if err := flooder.Register(ctx, name, id, windowExpired); err != nil {
				t.Fatalf("Flooder failed to register %s/%s/%v: %v", name, id, windowExpired, err)
			}
			// Verify event is not allowed.
			if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
				t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
			}

			// Clear as requested
			actualCleared, err1 := flooder.Clear(ctx, check.event, check.id)
			if err1 != nil {
				t.Fatalf("expected Clear to succeed: %#v", err1)
			}
			if actualCleared != check.expectedCleared {
				t.Fatalf("expected clear to return %d, got %d", check.expectedCleared, actualCleared)
			}
			if actualAllowed, err2 := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err2 != nil || actualAllowed != check.expectedAllowed {
				t.Fatalf("Expected ID allowed to be %t, got %t", check.expectedAllowed, actualAllowed)
			}
		})
	}
}

// This test is based on Drupal 9.3 FloodTest::testDatabaseBackend
func TestSQL_Drupal9Backend(t *testing.T) {
	ctx, flooder := testSQLSetup(t)
	defer flooder.Close()

	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || !isAllowed {
		t.Fatalf("Expected flooder to be allowed: %t, %#v", isAllowed, err)
	}
	// Register expired event.
	flooder.Register(ctx, name, id, windowExpired)
	// Verify event is not allowed.
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
		t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
	}
	// Run GarbageCollection and verify event is now allowed.
	flooder.GarbageCollection(ctx)
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || !isAllowed {
		t.Fatalf("Expected flooder to be allowed: %t, %#v", isAllowed, err)
	}

	// Register unexpired event.
	flooder.Register(ctx, name, id, flood.DefaultWindow)
	// Verify event is not allowed.
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
		t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
	}
	// Run GarbageCollection and verify event is still not allowed.
	flooder.GarbageCollection(ctx)
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
		t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
	}
}

// Verifies that having an ID be blocked for a name does not cause other IDs to
// be blocked for the same event name.
func TestSQL_Register2IDs(t *testing.T) {
	ctx, flooder := testSQLSetup(t)
	defer flooder.Close()

	const id1, id2 = "1", "2"
	// Register an ID for the name
	if err := flooder.Register(ctx, name, id1, windowExpired); err != nil {
		t.Fatalf("Failed to register %s/%s/%s: %v", name, id1, windowExpired, err)
	}
	// Verify it does not block other IDs, even nonexistent.
	if isAllowed, err := flooder.IsAllowed(ctx, name, id2, flood.DefaultWindow, 1); err != nil || !isAllowed {
		t.Fatalf("Expected flooder to be allowed: %t, %#v", isAllowed, err)
	}
}

func TestSQL_GarbageCollection(t *testing.T) {
	ctx, flooder := testSQLSetup(t)
	defer flooder.Close()

	ctx, cancel := context.WithCancel(ctx)
	flooder.Register(ctx, "gc", "127.0.0.1", 0)

	// GarbageCollection must clear the single pre-expired item.
	n, err := flooder.GarbageCollection(ctx)
	if err != nil {
		t.Fatalf("Unexpected GC failure: %#v", err)
	}
	if n != 1 {
		t.Fatalf("Expected GC to clear %d items, returned %d", 1, n)
	}

	// GarbageCollection must return 0 since no new value expired.
	n, err = flooder.GarbageCollection(ctx)
	if err != nil {
		t.Fatalf("Unexpected GC failure: %#v", err)
	}
	if n != 0 {
		t.Fatalf("Expected GC to clear %d items, returned %d", 0, n)
	}

	// GarbageCollection must fail if the context is canceled.
	cancel()
	_, err = flooder.GarbageCollection(ctx)
	if err == nil {
		t.Fatal("Unexpected GC success")
	}
}
