package flood

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"
)

// DefaultTableName is the default name for the table used as the storage for the SQL Flooder implementation.
const DefaultTableName = `flood`

// SQL is a RDBMS-backed Flooder implementation. See NewSQL.
type SQL struct {
	*sql.DB
	TableName string
}

func (s *SQL) Clear(ctx context.Context, name EventName, id ID) (cleared int, err error) {
	sql := `
DELETE FROM __TABLE__
WHERE event = ?
	AND id = ?
`
	sql = s.applyTable(sql)
	res, err := s.ExecContext(ctx, sql, name, id)
	if err != nil {
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	// XXX Assumes int larger or equal to int64.
	return int(count), ctx.Err()
}

// Close is only present to override the promoted DB.Close and avoid closing the
// underlying DB pool.
//
// It explicitly does not drop or truncate the associated storage, as other
// instances sharing the same storage may still be using it.
func (s *SQL) Close() error {
	return nil
}

func (s *SQL) GarbageCollection(ctx context.Context) (cleared int, err error) {
	sql := `
DELETE FROM __TABLE__
WHERE expires < ?
`
	sql = s.applyTable(sql)
	res, err := s.ExecContext(ctx, sql, s.sortableNanos(time.Now()))
	if err != nil {
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	// XXX Assumes int larger or equal to int64.
	return int(count), ctx.Err()
}

// IsAllowed implements flood.Flooder.
func (s *SQL) IsAllowed(ctx context.Context, name EventName, id ID, window time.Duration, threshold int) (bool, error) {
	sql := `
SELECT COUNT(*)
FROM __TABLE__
WHERE event = ?
	AND id = ?
	AND timestamp > ?
`
	sql = s.applyTable(sql)
	then := s.sortableNanos(time.Now().Add(-window))
	row := s.QueryRowContext(ctx, sql, name, id, then)
	var count int
	err := row.Scan(&count)
	if err != nil {
		return false, err
	}
	return count < threshold, ctx.Err()
}

func (s *SQL) Register(ctx context.Context, name EventName, id ID, window time.Duration) (err error) {
	// Fast path: assume table exists.
	if err = s.doInsert(ctx, name, id, window); err == nil {
		return nil
	}

	// No initial success: retry after ensuring table exists
	if err = s.ensureTableExists(ctx); err == nil {
		return s.doInsert(ctx, name, id, window)
	}

	return err
}

func (s *SQL) doInsert(ctx context.Context, name EventName, id ID, window time.Duration) error {
	// fid will be auto-generated.
	insert := `
INSERT INTO __TABLE__(event, id, timestamp, expires)
VALUES(?, ?, ?, ?)
`
	insert = s.applyTable(insert)
	now := time.Now()
	ts := s.sortableNanos(now)
	expires := s.sortableNanos(now.Add(window))
	_, err := s.ExecContext(ctx, insert, name, id, ts, expires)
	return err
}

func (s *SQL) applyTable(sql string) string {
	return strings.Replace(sql, `__TABLE__`, s.TableName, -1)
}

func (s *SQL) ensureTableExists(ctx context.Context) error {
	const (
		create = `
CREATE TABLE __TABLE__
(
    fid INTEGER PRIMARY KEY AUTOINCREMENT, -- Unique flood event ID
	event VARCHAR(64) NOT NULL,            -- Name of event (e.g. contact)
    id VARCHAR(128) NOT NULL,              -- Identifier of the visitor, such as an IP address or hostname
    timestamp CHAR(19) NOT NULL,           -- UTC timestamp of the event. See sortableNanos().
    expires CHAR(19) NOT NULL              -- UTC timestamp of expiration. Expired events are purged on GarbageCollection().
);
`
		allow = `
CREATE INDEX idx___TABLE___allow
ON __TABLE__ (event, id, timestamp);
`
		expires = `
CREATE INDEX idx___TABLE___expires
ON __TABLE__ (expires);
`
	)

	queries := [...]struct {
		sql    string
		format string
	}{
		{create, "failed to create table %s: %w"},
		{allow, "failed to create allow index on table %s: %w"},
		{expires, "failed to create allow index on table %s: %w"},
	}
	for _, query := range queries {
		ddl := s.applyTable(query.sql)
		_, err := s.ExecContext(ctx, ddl)
		if err != nil {
			return fmt.Errorf(query.format, s.TableName, err)
		}
	}
	return nil
}

// NewSQL creates a RDBMS-backed Flooder instance.
//
//  - db is an opened sql.DB pool
//  - tableName is the name of the table which the service will use.
//    If set to the empty string, it will use DefaultTableName.
func NewSQL(ctx context.Context, db *sql.DB, tableName string) *SQL {
	if tableName == `` {
		tableName = DefaultTableName
	}

	res := &SQL{DB: db, TableName: tableName}
	res.ensureTableExists(ctx)
	return res
}

// sortableNanos represents a time.Time as a 0-prefixed constant-length decimal
// numeric string representing the nanoseconds since the Unix epoch, suitable
// for text sort in SQL databases.
// This representation folds after 2262-04-11 23:47:16.854775807Z.
func (*SQL) sortableNanos(t time.Time) string {
	return fmt.Sprintf("%019d", t.UTC().UnixNano())
}
