// Copyright 2021 Frédéric G. MARAND
// Use of this source code is governed by the GPL-3-or-later
// license that can be found in the LICENSE.txt file at the repository root.

/*
Package flood provides a means to rate-limit events, based on the Flooder interface.

To use the service:

  - create a Flooder implementation using the New* functions.
    Bundled implementations include:
    - Memory, recommended for single-instance services not needing persistence.
    - SQL, recommended for any multi-instance service and those needing persistence.
      It uses standard SQL, on a standard Go sql.DB pool.

    Both implementations support concurrent operation.
  - On event occurrences, Flooder.Register them
  - To unblock an identity for an event name, use Flooder.Clear
  - To verify whether an event is allowed for an identity, use Flooder.IsAllowed
  - Periodically run Flooder.GarbageCollection to prune obsolete records.
  - Close the instance when done using it, probably with a defer.
*/
package flood
