package flood_test

import (
	"context"
	"testing"

	"gitlab.com/fgmarand/gopal/flood"
)

func testMemorySetup(_ *testing.T) (ctx context.Context, flooder flood.Flooder) {
	ctx = context.Background()
	flooder = flood.NewMemory()
	return ctx, flooder
}

func TestMemory_Clear(t *testing.T) {
	t.Parallel()
	checks := [...]struct {
		name            string
		event           flood.EventName
		id              flood.ID
		expectedCleared int
		expectedAllowed bool
	}{
		{"registered event+id", name, id, 1, true},
		{"unregistered event, registered id", name + name, id, 0, false},
		{"registered event, unregistered id", name, id + id, 0, false},
	}

	for _, check := range checks {
		check := check
		t.Run(check.name, func(t *testing.T) {
			t.Parallel()
			ctx, flooder := testMemorySetup(t)
			defer flooder.Close()

			// Register expired event.
			if err := flooder.Register(ctx, name, id, windowExpired); err != nil {
				t.Fatalf("Flooder failed to register %s/%s/%v: %v", name, id, windowExpired, err)
			}
			// Verify event is not allowed.
			if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
				t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
			}

			// Clear as requested
			actualCleared, err1 := flooder.Clear(ctx, check.event, check.id)
			if err1 != nil {
				t.Fatalf("expected Clear to succeed: %#v", err1)
			}
			if actualCleared != check.expectedCleared {
				t.Fatalf("expected clear to return %d, got %d", check.expectedCleared, actualCleared)
			}
			if actualAllowed, err2 := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err2 != nil || actualAllowed != check.expectedAllowed {
				t.Fatalf("Expected ID allowed to be %t, got %t", check.expectedAllowed, actualAllowed)
			}
		})
	}
}

// This test is based on Drupal 9.3 FloodTest::testMemoryBackend
func TestMemory_Drupal9Backend(t *testing.T) {
	t.Parallel()
	ctx, flooder := testMemorySetup(t)
	defer flooder.Close()

	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || !isAllowed {
		t.Fatalf("Expected flooder to be allowed: %t, %#v", isAllowed, err)
	}
	// Register expired event.
	flooder.Register(ctx, name, id, windowExpired)
	// Verify event is not allowed.
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
		t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
	}
	// Run GarbageCollection and verify event is now allowed.
	flooder.GarbageCollection(ctx)
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || !isAllowed {
		t.Fatalf("Expected flooder to be allowed: %t, %#v", isAllowed, err)
	}

	// Register unexpired event.
	flooder.Register(ctx, name, id, flood.DefaultWindow)
	// Verify event is not allowed.
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
		t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
	}
	// Run GarbageCollection and verify event is still not allowed.
	flooder.GarbageCollection(ctx)
	if isAllowed, err := flooder.IsAllowed(ctx, name, id, flood.DefaultWindow, threshold); err != nil || isAllowed {
		t.Fatalf("Expected flooder to be disallowed without error: %t, %#v", isAllowed, err)
	}
}

// Verifies that having an ID be blocked for a name does not cause other IDs to
// be blocked for the same event name.
func TestMemory_Register2IDs(t *testing.T) {
	t.Parallel()
	ctx, flooder := testMemorySetup(t)
	defer flooder.Close()

	const id1, id2 = "1", "2"
	// Register an ID for the name
	if err := flooder.Register(ctx, name, id1, windowExpired); err != nil {
		t.Fatalf("Failed to register %s/%s/%s: %v", name, id1, windowExpired, err)
	}
	// Verify it does not block other IDs, even nonexistent.
	if isAllowed, err := flooder.IsAllowed(ctx, name, id2, flood.DefaultWindow, 1); err != nil || !isAllowed {
		t.Fatalf("Expected flooder to be allowed: %t, %#v", isAllowed, err)
	}
}

func TestMemory_GarbageCollection(t *testing.T) {
	t.Parallel()
	ctx, flooder := testMemorySetup(t)
	defer flooder.Close()

	ctx, cancel := context.WithCancel(ctx)
	flooder.Register(ctx, "gc", "127.0.0.1", 0)

	// GarbageCollection must clear the single pre-expired item.
	n, err := flooder.GarbageCollection(ctx)
	if err != nil {
		t.Fatalf("Unexpected GC failure: %#v", err)
	}
	if n != 1 {
		t.Fatalf("Expected GC to clear %d items, returned %d", 1, n)
	}

	// GarbageCollection must return 0 since no new value expired.
	n, err = flooder.GarbageCollection(ctx)
	if err != nil {
		t.Fatalf("Unexpected GC failure: %#v", err)
	}
	if n != 0 {
		t.Fatalf("Expected GC to clear %d items, returned %d", 0, n)
	}

	// GarbageCollection must fail if the context is canceled.
	cancel()
	_, err = flooder.GarbageCollection(ctx)
	if err == nil {
		t.Fatal("Unexpected GC success")
	}
}
