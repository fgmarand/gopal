package flood_test

import "time"

const id = "127.0.0.1"
const name = "flood_test_cleanup"
const threshold int = 1
const windowExpired time.Duration = -1
