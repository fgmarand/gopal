package flood

import (
	"context"
	"io"
	"time"
)

type EventName string

// ID identifies the entity subject to flood control. In most scenarios, it
// will be the string representation of a user identity, or an IP address.
type ID string

// Flooder defines the flood API.
type Flooder interface {
	io.Closer

	// Clear deletes all recorded occurrences of an event for an ID.
	Clear(ctx context.Context, name EventName, id ID) (cleared int, err error)

	// GarbageCollection explicitly triggers the deletion of obsolete events.
	//
	// Some Flooder implementations may use an internal background expiration,
	// but not all do, so be sure to invoke this function periodically,
	// to ensure storage use does not grow indefinitely.
	GarbageCollection(ctx context.Context) (cleared int, err error)

	// IsAllowed verifies whether and event/ID pair is allowed to proceed.
	// It does not register said pair as having occurred.
	IsAllowed(ctx context.Context, name EventName, id ID, window time.Duration, threshold int) (bool, error)

	// Register allows registering the occurrence of a rate-limited event.
	//
	//   - If window < 0, the registration never expires.
	//     This implies that the event will never be garbage-collected:
	//     be sure to invoke Clear at some point for the event/ID pair.
	//   - If window == 0, it expires immediately, which is rarely useful.
	//   - The recommended value in most cases is DefaultWindow.
	Register(ctx context.Context, name EventName, id ID, window time.Duration) error
}

// DefaultWindow is the suggested flood duration.
const DefaultWindow = time.Hour
