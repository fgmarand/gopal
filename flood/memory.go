package flood

import (
	"context"
	"sync"
	"time"
)

type Memory struct {
	events map[EventName]map[ID]map[time.Time]time.Time
	sync.RWMutex
}

func NewMemory() *Memory {
	flooder := &Memory{
		events: make(map[EventName]map[ID]map[time.Time]time.Time),
	}
	return flooder
}

func (m *Memory) Clear(_ context.Context, name EventName, id ID) (cleared int, err error) {
	m.Lock()
	defer m.Unlock()
	namedEvents, ok := m.events[name]
	if !ok {
		return 0, nil
	}
	idEvents, ok := namedEvents[id]
	if !ok {
		return 0, nil
	}
	l := len(idEvents)
	delete(m.events[name], id)
	return l, nil
}

func (m *Memory) Close() error {
	m.Lock()
	m.events = NewMemory().events
	m.Unlock()
	return nil
}

func (m *Memory) GarbageCollection(ctx context.Context) (cleared int, err error) {
	l := 0
	m.Lock()
	defer m.Unlock()
	now := time.Now()
	for name, namedEvents := range m.events {
		for id, idEvents := range namedEvents {
			for expires := range idEvents {
				if expires.Before(now) {
					delete(m.events[name][id], expires)
					l++
				}
			}
			if len(m.events[name][id]) == 0 {
				delete(m.events[name], id)
			}
		}
		if len(m.events[name]) == 0 {
			delete(m.events, name)
		}
	}
	return l, ctx.Err()
}

func (m *Memory) IsAllowed(ctx context.Context, name EventName, id ID, window time.Duration, threshold int) (bool, error) {
	m.RLock()
	defer m.RUnlock()
	var ok bool
	if _, ok = m.events[name]; !ok {
		return threshold > 0, nil
	}
	var idEvents map[time.Time]time.Time
	if idEvents, ok = m.events[name][id]; !ok {
		return threshold > 0, nil
	}
	limit := time.Now().Add(-window)
	count := 0
	for _, registered := range idEvents {
		if registered.After(limit) {
			count++
		}
	}
	return count > threshold, ctx.Err()
}

func (m *Memory) Register(ctx context.Context, name EventName, id ID, window time.Duration) error {
	m.Lock()
	defer m.Unlock()
	if _, ok := m.events[name]; !ok {
		m.events[name] = make(map[ID]map[time.Time]time.Time)
	}
	if _, ok := m.events[name][id]; !ok {
		m.events[name][id] = make(map[time.Time]time.Time)
	}
	// In the unlikely case of a time collision, the result is correct: we only
	// need a single entry for a given flood termination time.
	m.events[name][id][time.Now().Add(window)] = time.Now()
	return ctx.Err()
}
