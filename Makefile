PACKAGES := $(shell go list ./... | grep -v /vendor/)
clean:
	find . -type f -name test.db -o -name coverage.txt -exec rm {} \;

lint:
	@echo Running go fmt
	@echo $(PACKAGES) | xargs go fmt
	@echo Running go vet
	@echo $(PACKAGES) | xargs go vet
	staticcheck ./flood/... ./ban/...

cover:
	@echo $(PACKAGES) | xargs -t go test -race -tags sqlite -coverprofile=coverage.txt -covermode=atomic

test:
	@echo $(PACKAGES) | xargs -t go test -v -race

test/db:
	@echo $(PACKAGES) | xargs -t go test -v -race -tags mysql,postgresql,sqlite
