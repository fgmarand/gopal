//go:build sqlite

package ban_test

import (
	"context"
	"database/sql"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/fgmarand/gopal/ban"
)

const SQLiteTestPath = "test.db"

func testSQLSQLiteSetup(t *testing.T) (ctx context.Context, banner *ban.SQL) {
	ctx = context.Background()
	os.RemoveAll(SQLiteTestPath)
	db, err := sql.Open(`sqlite3`, SQLiteTestPath)
	if err != nil {
		t.Fatalf("could not open a SQLite test instance in %s: %v", SQLiteTestPath, err)
	}
	banner = ban.NewSQL(ctx, db, "")
	return ctx, banner
}
