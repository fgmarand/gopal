package ban_test

import (
	"net"
	"testing"

	"gitlab.com/fgmarand/gopal/ban"
)

const maxBans = 10

var testIP = net.ParseIP("127.0.0.1")

func TestBan_String(t *testing.T) {
	expected := `{"ID":0x1, "IP":"127.0.0.1"}`
	ip := testIP
	actual := ban.Ban{ID: 1, IP: ip}.String()
	if actual != expected {
		t.Errorf("expected %s, got %s", expected, actual)
	}
}
