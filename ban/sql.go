package ban

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net"
	"reflect"
	"strings"

	"github.com/go-sql-driver/mysql"
	"github.com/mattn/go-sqlite3"
)

// DefaultTableName is the default name for the table used as the storage for the SQL Flooder implementation.
const DefaultTableName = `ban`

// SQL is a RDBMS-backed Banner implementation. See NewSQL.
// In the current version, it only supports SQLIte > 3.
// TODO support other engines.
type SQL struct {
	*sql.DB
	flavor      string
	TableName   string
	overrideBan func(*SQL, context.Context, net.IP) (ID, error)
	overrideETE func(*SQL, context.Context) error
	overrideGCD func(*SQL, context.Context) (string, error)
	overrideITE func(*SQL, context.Context, string) (bool, error)
}

// Ban upserts an IP to the ban list.
func (s *SQL) Ban(ctx context.Context, ip net.IP) (ID, error) {
	if s.overrideBan == nil {
		return 0, fmt.Errorf("no implementation found for ban/%s", s.flavor)
	}
	id, err := s.overrideBan(s, ctx, ip)
	switch err {
	case ErrAlreadyBanned:
		return id, ErrAlreadyBanned
	case nil:
		return id, ctx.Err()
	default:
		return 0, fmt.Errorf("banning %s in %s: %w", ip, s.flavor, err)
	}
}

// Close is only present to override the promoted DB.Close and avoid closing the
// underlying DB pool.
//
// It explicitly does not drop or truncate the associated storage, as other
// instances sharing the same storage may still be using it.
func (s *SQL) Close() error {
	return nil
}

func (s *SQL) FindAll(ctx context.Context) ([]Ban, error) {
	selector := `
SELECT id, ip
FROM __TABLE__;
`
	selector = s.applyTable(selector)
	rows, err := s.QueryContext(ctx, selector)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	bans := make([]Ban, 0, DefaultBanListSize)
	for rows.Next() {
		ban := Ban{}
		var sIP string
		err := rows.Scan(&ban.ID, &sIP)
		if err != nil {
			return nil, err
		}
		ban.IP = net.ParseIP(sIP)
		if ban.IP == nil {
			return bans, ErrInvalidIP
		}
		bans = append(bans, ban)
	}
	if err = rows.Err(); err != nil {
		return bans, err
	}
	return bans, ctx.Err()
}

func (s *SQL) FindByID(ctx context.Context, id ID) (net.IP, error) {
	selector := `
SELECT ip
FROM __TABLE__
WHERE id = ?;
`
	selector = s.applyTable(selector)
	row := s.QueryRowContext(ctx, selector, id)
	var sIP string
	err := row.Scan(&sIP)
	switch err {
	case sql.ErrNoRows:
		return nil, ErrNonExistentBan
	case nil:
		ip := net.ParseIP(sIP)
		if ip == nil {
			return nil, ErrInvalidIP
		}
		return ip, ctx.Err()
	default:
		return nil, err
	}
}

func (s *SQL) findByIP(ctx context.Context, ip net.IP) (*Ban, error) {
	selector := `
SELECT id
FROM __TABLE__
WHERE ip = ?
`
	selector = s.applyTable(selector)
	row := s.QueryRowContext(ctx, selector, ip.String())
	var id ID
	err := row.Scan(&id)
	switch err {
	case sql.ErrNoRows:
		return nil, ErrNonExistentBan
	case nil:
		return &Ban{ID: id, IP: ip}, ctx.Err()
	default:
		return nil, err
	}
}

func (s *SQL) IsBanned(ctx context.Context, ip net.IP) (bool, error) {
	_, err := s.findByIP(ctx, ip)
	switch err {
	case ErrNonExistentBan:
		return false, ctx.Err()
	case nil:
		return true, ctx.Err()
	default:
		return false, err
	}
}

func (s *SQL) UnBan(ctx context.Context, ip net.IP) error {
	dml := `
DELETE FROM __TABLE__
WHERE ip = ?
`
	dml = s.applyTable(dml)
	res, err := s.ExecContext(ctx, dml, ip.String())
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return ErrNonExistentBan
	}
	return ctx.Err()
}

func (s *SQL) applyTable(sql string) string {
	return strings.Replace(sql, `__TABLE__`, s.TableName, -1)
}

func (s *SQL) ensureTableExists(ctx context.Context) error {
	if s.overrideETE == nil {
		return fmt.Errorf("no implementation found for ensureTableExists/%s", s.flavor)
	}
	return s.overrideETE(s, ctx)
}

func (s *SQL) isTableExistent(ctx context.Context) (bool, error) {
	var dbName string
	var err error
	var isIt bool
	if s.overrideGCD != nil {
		if dbName, err = s.overrideGCD(s, ctx); err != nil {
			return false, fmt.Errorf("getting current database name (%s): %w", s.flavor, err)
		}
	}
	if s.overrideITE != nil {
		if isIt, err = s.overrideITE(s, ctx, dbName); err != nil {
			return false, err
		}
		return isIt, nil
	}
	return false, fmt.Errorf("no implementation for getCurrentDatabase in %s", s.flavor)
}

// NewSQL creates a RDBMS-backed Banner instance.
//
//  - db is an opened sql.DB pool
//  - tableName is the name of the table which the service will use.
//    If set to the empty string, it will use DefaultTableName.
func NewSQL(ctx context.Context, db *sql.DB, tableName string) *SQL {
	if tableName == `` {
		tableName = DefaultTableName
	}

	res := &SQL{DB: db, TableName: tableName}
	switch db.Driver().(type) {
	case *mysql.MySQLDriver:
		res.flavor = "mysql"
		res.overrideBan = mysqlBan
		res.overrideETE = mysqlEnsureTableExists
		res.overrideGCD = mysqlGetCurrentDatabase
		res.overrideITE = mysqlIsTableExistent
	case *sqlite3.SQLiteDriver:
		res.flavor = "sqlite3"
		res.overrideBan = sqliteBan
		res.overrideETE = sqliteEnsureTableExists
	default:
		log.Fatalf("Unsupported driver %s", reflect.TypeOf(db.Driver()).String())
	}

	if err := res.ensureTableExists(ctx); err != nil {
		log.Fatalf("Could not ensure table %s existed: %v", res.TableName, err)
	}
	return res
}
