//go:build !sqlite

package ban_test

import (
	"context"
	"testing"

	"gitlab.com/fgmarand/gopal/ban"
)

const SQLiteTestPath = "test.db"

func testSQLSQLiteSetup(t *testing.T) (context.Context, *ban.SQL) {
	return context.Background(), nil
}
