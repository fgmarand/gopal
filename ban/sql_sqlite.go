package ban

import (
	"context"
	"fmt"
	"net"

	"github.com/mattn/go-sqlite3"
)

func sqliteBan(s *SQL, ctx context.Context, ip net.IP) (ID, error) {
	// id will be auto-generated.
	insert := `
INSERT INTO __TABLE__(ip)
VALUES(?)
`
	insert = s.applyTable(insert)
	tx, err := s.BeginTx(ctx, nil)
	if err != nil {
		return 0, err
	}
	res, err := tx.ExecContext(ctx, insert, ip.String())
	if err != nil {
		defer tx.Rollback()
		sqliteErr, ok := err.(sqlite3.Error)
		// SQLite error for unique constraint violation.
		if ok && sqliteErr.Code == sqlite3.ErrConstraint && sqliteErr.ExtendedCode == sqlite3.ErrConstraintUnique {
			ban, err := s.findByIP(ctx, ip)
			switch err {
			case ErrNonExistentBan:
				// This should not happen: we entered this case because there
				// was an existing ID for that IP.
				return 0, ErrStorageInconsistency
			case nil:
				return ban.ID, ErrAlreadyBanned
			default:
				return 0, err
			}
		}
		return 0, err
	}
	tx.Commit()
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return ID(id), ctx.Err()
}

func sqliteEnsureTableExists(s *SQL, ctx context.Context) error {
	const (
		create = `
CREATE TABLE __TABLE__
(
	
	 
    id INTEGER PRIMARY KEY AUTOINCREMENT, -- Unique ban event ID 
    ip VARCHAR(45) NOT NULL               -- https://stackoverflow.com/a/166157/33991               
);
`
		ip = `
CREATE UNIQUE INDEX uix___TABLE___ip
ON __TABLE__ (ip);
`
	)
	queries := [...]struct {
		sql    string
		format string
	}{
		{create, "failed to create table %s: %w"},
		{ip, "failed to create IP index on table %s: %w"},
	}
	for _, query := range queries {
		ddl := s.applyTable(query.sql)
		_, err := s.ExecContext(ctx, ddl)
		if err != nil {
			return fmt.Errorf(query.format, s.TableName, err)
		}
	}
	return nil
}
