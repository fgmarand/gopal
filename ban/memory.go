package ban

import (
	"context"
	"net"
	"sync"
	"sync/atomic"
)

// Memory is an in-memory non-persistent implementation of ban.Banner.
// The blank value is a ready-to-use instance.
//
// There is no guarantee that a once-Closed instance may be reused, although
// this is the case in the current implementation.
type Memory struct {
	// Map is used here because each entry is expected to be written only once,
	// when banning (and deleted one when unbanning) but read ofte to check access.
	// Keys are IPs in string format (because net.IP cannot he hashed), values are IDs.
	sync.Map

	// lastID is the last allocated ID. It is not necessarily still present in the ID list.
	lastID ID
}

func (m *Memory) Close() error {
	m.Map = sync.Map{}
	return nil
}

func (m *Memory) Ban(_ context.Context, ip net.IP) (ID, error) {
	idAny, loaded := m.LoadOrStore(ip.String(), m.newID())
	id, ok := idAny.(ID)
	if !ok {
		return 0, ErrInvalidID
	}
	if loaded {
		return id, ErrAlreadyBanned
	}
	return id, nil
}

func (m *Memory) FindAll(ctx context.Context) ([]Ban, error) {
	all := make([]Ban, 0, DefaultBanListSize)
	var err error
	m.Range(func(k, v interface{}) bool {
		sIP, ok := k.(string)
		if !ok {
			err = ErrInvalidIP
			return false
		}
		ip := net.ParseIP(sIP)
		if ip == nil {
			err = ErrInvalidIP
			return false
		}

		id, ok := v.(ID)
		if !ok {
			err = ErrInvalidID
			return false
		}
		all = append(all, Ban{ID: id, IP: ip})
		if ctxErr := ctx.Err(); ctxErr != nil {
			err = ctxErr
			return false
		}
		return true
	})
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (m *Memory) FindByID(ctx context.Context, id ID) (net.IP, error) {
	found := false
	var err error
	var ip net.IP
	m.Range(func(k, v interface{}) bool {
		rangeID, ok := v.(ID)
		if !ok {
			err = ErrInvalidID
			return false
		}
		if rangeID != id {
			return true
		}

		sIP, ok := k.(string)
		if !ok {
			err = ErrInvalidIP
			return false
		}
		ip = net.ParseIP(sIP)
		if ip == nil {
			err = ErrInvalidIP
			return false
		}
		found = true
		return false
	})
	switch {
	case !found:
		return net.IP{}, ErrNonExistentBan
	case err != nil: // Storage corruption errors.
		return net.IP{}, err
	default:
		return ip, ctx.Err()
	}
}

func (m *Memory) IsBanned(ctx context.Context, ip net.IP) (bool, error) {
	_, banned := m.Load(ip.String())
	return banned, ctx.Err()
}

func (m *Memory) UnBan(ctx context.Context, ip net.IP) error {
	any, loaded := m.LoadAndDelete(ip.String())
	if !loaded {
		return ErrNonExistentBan
	}
	_, ok := any.(ID)
	if !ok {
		return ErrInvalidID
	}
	return ctx.Err()
}

func (m *Memory) newID() ID {
	return ID(atomic.AddUint64((*uint64)(&m.lastID), 1))
}
