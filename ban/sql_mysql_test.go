//go:build mysql

package ban_test

import (
	"context"
	"database/sql"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/fgmarand/gopal/ban"
)

const MySQLDSNVar = "GOPAL_TEST_DSN_MYSQL"
const MySQLDefaultDSN = "tcp(localhost)/gopal_test"

func testSQLMySQLSetup(t *testing.T) (context.Context, *ban.SQL) {
	dsn := os.Getenv(MySQLDSNVar)
	if dsn == "" {
		dsn = MySQLDefaultDSN
	}
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		t.Fatalf("Failed opening MySQL connection: %v", err)
	}
	ctx := context.Background()
	banner := ban.NewSQL(ctx, db, "")
	return ctx, banner
}
