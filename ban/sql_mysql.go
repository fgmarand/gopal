package ban

import (
	"context"
	"database/sql"
	"fmt"
	"net"

	"github.com/go-sql-driver/mysql"
)

const (
	/* https://mariadb.com/kb/en/mariadb-error-codes/ */
	ErDupEntry = 1062
)

func mysqlBan(s *SQL, ctx context.Context, ip net.IP) (ID, error) {
	// id will be auto-generated.
	insert := `
INSERT INTO __TABLE__(ip)
VALUES(?)
`
	insert = s.applyTable(insert)
	tx, err := s.BeginTx(ctx, nil)
	if err != nil {
		return 0, err
	}
	res, err := tx.ExecContext(ctx, insert, ip.String())
	if err != nil {
		defer tx.Rollback()
		myErr, ok := err.(*mysql.MySQLError)
		// SQLite error for unique constraint violation.
		if ok && myErr.Number == ErDupEntry {
			ban, err := s.findByIP(ctx, ip)
			switch err {
			case ErrNonExistentBan:
				// This should not happen: we entered this case because there
				// was an existing ID for that IP.
				return 0, ErrStorageInconsistency
			case nil:
				return ban.ID, ErrAlreadyBanned
			default:
				return 0, err
			}
		}
		return 0, err
	}
	tx.Commit()
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return ID(id), ctx.Err()
}

func mysqlGetCurrentDatabase(s *SQL, ctx context.Context) (string, error) {
	const selector = "SELECT DATABASE();"
	row := s.QueryRowContext(ctx, selector)
	var name string
	if err := row.Scan(&name); err != nil {
		return "", err
	}
	return name, nil
}

func mysqlIsTableExistent(s *SQL, ctx context.Context, dbName string) (bool, error) {
	dml := `
SELECT table_rows
FROM information_schema.tables t
WHERE t.table_schema = ?
  AND t.TABLE_NAME = ?
`
	row := s.QueryRowContext(ctx, dml, dbName, s.TableName)
	var count int
	err := row.Scan(&count)
	switch {
	case err == sql.ErrNoRows:
		return false, nil
	case err == nil:
		return count != 0, nil
	default:
		return false, err
	}
}

func mysqlEnsureTableExists(s *SQL, ctx context.Context) error {
	var tableExists bool
	var err error
	if tableExists, err = s.isTableExistent(ctx); err != nil {
		return err
	}
	const drop = `
DROP TABLE __TABLE__;
`
	const create = `
CREATE TABLE __TABLE__ (
  id int(11) NOT NULL AUTO_INCREMENT,
  ip varchar(45) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY uix_ban_ip (ip)
);
`
	type check struct {
		sql    string
		format string
	}
	var queries = make([]check, 0, 2)
	if tableExists {
		queries = append(queries, check{drop, "failed to drop existing table %s: %w"})
	}
	queries = append(queries, check{create, "failed to create table %s: %w"})
	for _, query := range queries {
		ddl := s.applyTable(query.sql)
		_, err := s.ExecContext(ctx, ddl)
		if err != nil {
			return fmt.Errorf(query.format, s.TableName, err)
		}
	}
	return nil
}
