// Copyright 2021 Frédéric G. MARAND
// Use of this source code is governed by the GPL-3-or-later
// license that can be found in the LICENSE.txt file at the repository root.

/*
Package ban provides an IP ban service, based on the Banner interface.
Expected usage is as a http server middleware.

To use the service:

  - create a Banner implementation.
    Bundled implementations include:
    - Memory, recommended for single-instance services not needing persistence.
	- SQL, recommended for multi-instance services or those needing persistence.
    All implementations support concurrent operation.
  - On request handling, check the IP with Banner.IsBanned.
  - To ban an IP, use Banner.Ban
  - To unban an IP, use Banner.UnBan
  - Management UIs/debugging can list bans with Banner.FindAll ad Banner.FindByID
  - Close the instance when done using it, probably with a defer.
*/
package ban
