package ban

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
)

type ID uint64

type Ban struct {
	ID
	net.IP
}

func (b Ban) String() string {
	return fmt.Sprintf(`{"ID":0x%0x, "IP":"%s"}`, b.ID, b.IP)
}

// Banner is the interface defining the ability to ban IPs.
//
// It has nothing to do with advertising banners or the IAB.
type Banner interface {
	// Closer allows closing the Banner instance.
	//
	// The interface does not define persistence considerations on a closed instance:
	// some implementations may persist (e.g. SQL), while others will not (e.g. Memory).
	io.Closer

	// Ban adds an IP to the ban list.
	//
	// Although it is idempotent at the storage level, repeated calls for the
	// same IP will return ErrAlreadyBanned, and should be seen as a logic error.
	Ban(ctx context.Context, ip net.IP) (ID, error)

	// FindAll may return large slices, and is very inefficient in the general case:
	// because of the concurrent access property, it only represents a crawl through
	// the ban storage, which may not represent an actual snapshot of that storage at any time.
	//
	// As such, it is considered an unstable function, mostly be seen as a debugging tool,
	// which could be used for admin UIs on sites with smaller ban lists.
	FindAll(ctx context.Context) ([]Ban, error)

	// FindByID is an administrative operation, which should not be used in the
	// hot path, as it may be highly inefficient (O(n) by number of IPs).
	//
	// For most situations, access the storage using IsBanned instead.
	FindByID(ctx context.Context, id ID) (net.IP, error)

	// IsBanned is the only operation meant for the hot path, and is expected to
	// the most used one, by far.
	IsBanned(ctx context.Context, ip net.IP) (bool, error)

	// UnBan removes an IP from the ban list.
	//
	// Although it is idempotent at the storage level, repeated calls for the
	// same IP will return ErrNonExistentBan, and should be seen as a logic error.
	UnBan(ctx context.Context, ip net.IP) error
}

const (
	// DefaultBanListSize is a heuristic for storage sizing only.
	DefaultBanListSize = 50
)

var (
	// ErrAlreadyBanned is a logic error, but without consequences. It may be ignored.
	ErrAlreadyBanned = errors.New("already banned")

	// ErrInvalidID  is a severe error, meaning data corruption.
	// Program should stop, or at the very least reset the Banner instance.
	ErrInvalidID = errors.New("ID is invalid")

	// ErrInvalidIP  is a severe error, meaning data corruption.
	// Program should stop, or at the very least reset the Banner instance.
	ErrInvalidIP = errors.New("IP is invalid")

	// ErrNonExistentBan is a logic error, but without consequences. It may be ignored.
	ErrNonExistentBan = errors.New("non-existent ban")

	// ErrStorageInconsistency  is a severe error, meaning data corruption.
	// Program should stop, or at the very least reset the Banner instance.
	ErrStorageInconsistency = errors.New("storage inconsistency")
)
