package ban_test

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"net"
	"testing"
	"time"

	"gitlab.com/fgmarand/gopal/ban"
)

func makeSQLChecks() []struct {
	name  string
	setup func(t *testing.T) (context.Context, *ban.SQL)
} {
	checks := []struct {
		name  string
		setup func(t *testing.T) (context.Context, *ban.SQL)
	}{
		{"mysql", testSQLMySQLSetup},
		{"sqlite", testSQLSQLiteSetup},
	}
	return checks
}

func setupSQLBans(ctx context.Context, banner ban.Banner, t *testing.T) map[ban.ID]net.IP {
	seen := make(map[ban.ID]net.IP, maxBans)
	for i := 1; i <= maxBans; i++ {
		sIP := fmt.Sprintf("127.0.0.%d", i)
		ip := net.ParseIP(sIP)
		if ip == nil {
			t.Fatalf("unexpected error setting up test IP %s", sIP)
		}
		id, err := banner.Ban(ctx, ip)
		if err != nil {
			t.Fatalf("failed banning %s: %v", ip, err)
		}
		_, alreadySeen := seen[id]
		if alreadySeen {
			t.Fatalf("different bands return same id: %d", id)
		}
		seen[id] = ip
	}
	if l := len(seen); l != maxBans {
		t.Fatalf("expected %d seen ids, got %d", maxBans, l)
	}
	return seen
}

func TestSQL_Ban_IsBanned(t *testing.T) {
	for _, check := range makeSQLChecks() {
		check := check
		t.Run(check.name, func(t *testing.T) {
			ctx, banner := check.setup(t)
			if banner == nil {
				t.Skipf("Skipped: no %s build tag", check.name)
			}
			defer banner.Close()
			if isBanned, err := banner.IsBanned(ctx, testIP); err != nil || isBanned {
				t.Fatalf("unexpected error or ban for test IP: %t, %v", isBanned, err)
			}
			id1, err := banner.Ban(ctx, testIP)
			if err != nil {
				t.Fatalf("unexpected error banning test IP: %v", err)
			}
			id2, err := banner.Ban(ctx, testIP)
			if !errors.Is(err, ban.ErrAlreadyBanned) {
				t.Fatalf("unexpected error banning test IP again: %v", err)
			}
			if id1 != id2 {
				t.Fatalf("expected repeated banning to return the same ID: %d vs %d", id1, id2)
			}
		})
	}
}

func TestSQL_Close(t *testing.T) {
	for _, check := range makeSQLChecks() {
		check := check
		t.Run(check.name, func(t *testing.T) {
			ctx, banner := check.setup(t)
			if banner == nil {
				t.Skipf("Skipped: no %s build tag", check.name)
			}
			defer banner.Close()
			_, err := banner.Ban(ctx, testIP)
			if err != nil {
				t.Fatalf("unexpected error banning test IP: %v", err)
			}
			banner.Close()
			isBanned, err := banner.IsBanned(ctx, testIP)
			if err != nil {
				t.Fatalf("unexpected error checking test IP: %v", err)
			}
			if !isBanned {
				t.Fatalf("unexpected non-persisted ban after a SQL.Close")
			}
		})
	}
}

func TestSQL_FindAll(t *testing.T) {
	for _, check := range makeSQLChecks() {
		check := check
		t.Run(check.name, func(t *testing.T) {
			ctx, banner := check.setup(t)
			if banner == nil {
				t.Skipf("Skipped: no %s build tag", check.name)
			}
			defer banner.Close()
			seen := setupSQLBans(ctx, banner, t)

			all, err := banner.FindAll(ctx)
			if err != nil {
				t.Fatalf("unexpected error finding all bans: %v", err)
			}
			if l := len(all); l != maxBans {
				t.Fatalf("expected %d entries, got %d", maxBans, l)
			}
			for _, ban := range all {
				if byte(ban.ID) != ban.IP.To4()[3] {
					t.Errorf("Ban IP %s does not match ID %d", ban.IP, ban.ID)
				}
				delete(seen, ban.ID)
			}
			if len(seen) != 0 {
				t.Fatalf("Expected all seen bans to have been cleared. Remaining: %v", seen)
			}
		})
	}
}

func TestSQL_FindByID(t *testing.T) {
	for _, check := range makeSQLChecks() {
		check := check
		t.Run(check.name, func(t *testing.T) {
			ctx, banner := check.setup(t)
			if banner == nil {
				t.Skipf("Skipped: no %s build tag", check.name)
			}
			defer banner.Close()
			seen := setupSQLBans(ctx, banner, t)

			rand.Seed(time.Now().UnixMicro())
			id := ban.ID(1 + rand.Int31n(maxBans-1)) // IDs go from 1 to maxBans.

			// Happy path
			ip, err := banner.FindByID(ctx, id)
			if err != nil {
				t.Fatalf("unexpected error finding ban %d by ID: %v", id, err)
			}
			if ip.String() != seen[id].String() {
				t.Fatalf("unexpected IP for ID %d: %s", id, ip)
			}

			// Sad path: missing ban
			_, err = banner.FindByID(ctx, id+maxBans)
			if err != ban.ErrNonExistentBan {
				t.Fatalf("unexpected error finding ban %d by ID: %v", id, err)
			}
		})
	}
}

func TestSQL_UnBan(t *testing.T) {
	for _, check := range makeSQLChecks() {
		check := check
		t.Run(check.name, func(t *testing.T) {
			ctx, banner := check.setup(t)
			if banner == nil {
				t.Skipf("Skipped: no %s build tag", check.name)
			}
			defer banner.Close()
			seen := setupSQLBans(ctx, banner, t)

			rand.Seed(time.Now().UnixMicro())
			id := ban.ID(1 + rand.Int31n(maxBans-1)) // IDs go from 1 to maxBans.
			ip := seen[id]

			isBanned, err := banner.IsBanned(ctx, ip)
			if err != nil {
				t.Fatalf("unexpected error checking ban for %s: %v", ip, err)
			}
			if !isBanned {
				t.Fatalf("expected %s to be banned before unban", ip)
			}

			// Happy path.
			err = banner.UnBan(ctx, ip)
			if err != nil {
				t.Fatalf("unexpected error unbanning %s: %v", ip, err)
			}

			// Sad path
			err = banner.UnBan(ctx, ip)
			if err != ban.ErrNonExistentBan {
				t.Fatalf("unexpected result unbanning %s: %v", ip, err)
			}
		})
	}
}
