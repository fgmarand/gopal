[![Go Reference](https://pkg.go.dev/badge/gitlab.com/fgmarand/gopal.svg)](https://pkg.go.dev/gitlab.com/fgmarand/gopal)
[![codecov](https://codecov.io/gl/fgmarand/gopal/branch/ci/graph/badge.svg?token=MAU3ER5VC9)](https://codecov.io/gl/fgmarand/gopal)

# Gopal

Gopal is a set of Go packages inspired by Drupal features (D8 and later), but built from scratch in Go.

Its purpose is NOT to rebuild Drupal in Go, but to provide some equivalent services to Go backend developers.

It does NOT contain any actual Drupal code: the backend code is in Go instead of PHP, and built quite differently; and at this point it does not contain any front-end code

## Components
### Working

- [ban](ban) is based on the Drupal [ban](https://api.drupal.org/api/drupal/namespace/Drupal%21ban/9.3.x) API.
- [flood](flood) is based on the Drupal [flood](https://api.drupal.org/api/drupal/namespace/Drupal%21ban/9.3.x) API.

### Still in progress

- [language](language)


## Check code quality

Use the `make lint` command, which requires some tools to be installed:

- `go fmt` and `go vet`, both shipped in the Go SDK
- [StaticCheck](https://staticcheck.io)


## How to run tests and generate coverage

- `make test` will run tests without any database engine
- `make test/db` will run tests without any database engine using all supported database engines
  - SQLite: creates default `test.db` databases, needs disk write access
  - MySQL: 
    - start by creating needs a MySQL-compatible database
    - it may be a passwordless database on localhost, called `gopal_test`
    - or define its DSN defined in environment variable `GOPAL_TEST_DSN_MYSQL`, like this example, where the quoting is required to prevent the shell from interpreting the parentheses:
```shell
$ export GOPAL_TEST_DSN_MYSQL="user:pass@tcp(host)/base"
$ make test/db
```
- the usual `go test` command can be used to target tests more specifically, using the same environment variable, and allowing to select which database tests are run:
```shell
$ go test -race -tags sqlite # Only run tests on SQLite
$ # Define a MySQL/MariaDB database and test on it too
$ export GOPAL_TEST_DSN_MYSQL="user:pass@tcp(host)/base"
$ go test -race -tags mysql,sqlite
```
- `make cover` will generate coverage in `coverage.txt`, only running SQL tests on SQLite.
- `make clean` cleans up coverage and potential leftover SQLite databases.

## IP / Licensing

- This code is copyright (c) 2014-2021 Frédéric G. MARAND.
- It is published under the General Public License, version 3 or later (SPDX: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html))
