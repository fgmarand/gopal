/*
Package gopal is the root of a set of packages for backend development,
inspired by the Drupal 9/8 CMS.

See :
  - https://gitlab.com/fgmarand/gopal/-/blob/main/README.md
  - https://drupal.org
  - https://fr.wikipedia.org/wiki/Gopal
*/
package gopal

type Uuid string

type Weight int8
