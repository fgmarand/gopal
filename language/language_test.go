package language

import (
	"sort"
	"testing"

	. "gitlab.com/fgmarand/gopal"
	gt "gitlab.com/fgmarand/gopal/testing"
)

// Covers Language::Name()
func TestName(t *testing.T) {
	name := gt.RandomMachineName(8)
	language_code := gt.RandomMachineName(2)

	language := Language{
		id:   language_code,
		name: name,
	}

	expected := name
	actual := language.Name()
	if expected != actual {
		t.Errorf("Name = %v, want %v", expected, actual)
	}
}

// Covers Language.Id()
func TestId(t *testing.T) {
	language_code := gt.RandomMachineName(2)
	language := Language{
		id: language_code,
	}

	expected := language_code
	actual := language.Id()
	if expected != actual {
		t.Errorf("Id = %v, want %v", expected, actual)
	}
}

// Covers Language.Direction()
func TestDirection(t *testing.T) {
	language_code := gt.RandomMachineName(2)
	language := Language{
		id:        language_code,
		direction: DIRECTION_RTL,
	}

	expected := DIRECTION_RTL
	actual := language.Direction()
	if expected != actual {
		t.Errorf("Direction = %v, want %v", expected, actual)
	}
}

// Covers Language.IsDefault()
func TestIsDefault(t *testing.T) {
	language_code := gt.RandomMachineName(2)
	language := Language{
		id:        language_code,
		isDefault: true,
	}

	expected := true
	actual := language.IsDefault()
	if expected != actual {
		t.Errorf("Default = %v, want %v", expected, actual)
	}
}

func TestWeight(t *testing.T) {
	var expected Weight = 42
	language_code := gt.RandomMachineName(2)
	language := Language{
		id:     language_code,
		weight: expected,
	}

	actual := language.Weight()
	if expected != actual {
		t.Errorf("Direction = %v, want %v", expected, actual)
	}
}

/* Tests sorting an array of language objects.

Covers methods implementing sort.Interface
*/
func TestSortArrayOfLanguages(t *testing.T) {
	description, actual, expected := providerTestSortArrayOfLanguages()

	for key_list, value_list := range actual {
		sort.Sort(value_list)
		for key_lang, value_lang := range value_list {
			expected_lang := expected[key_list][key_lang]
			if value_lang != expected_lang {
				t.Fail()
				t.Errorf("For case %s, offset %d , expected %v, got %v", description[key_list], key_lang, value_lang, expected_lang)
			}
		}
	}
}

// Provides data for testSortArrayOfLanguages.
func providerTestSortArrayOfLanguages() ([]string, []Languages, []Languages) {
	language09A := Language{id: "dd", name: "A", weight: 9}
	language10A := Language{id: "ee", name: "A", weight: 10}
	language10B := Language{id: "ff", name: "B", weight: 10}

	description := []string{
		"#0: already ordered by weight",
		"#1: out of order by weight",
		"#2: tied by weight, already ordered by name",
		"#3: tied by weight, out of order by name",
	}

	actual := []Languages{
		{language09A, language10B},
		{language10B, language09A},
		{language10A, language10B},
		{language10B, language10A},
	}

	expected := []Languages{
		{language09A, language10B},
		{language09A, language10B},
		{language10A, language10B},
		{language10A, language10B},
	}

	return description, actual, expected
}
