package language

import . "gitlab.com/fgmarand/gopal"

// Language codes
const (
	/* Special system language code (only applicable to UI language).

	Refers to the language used in source code. CMS uses the built-in text for
	English by default, but if configured to allow translation/customization of
	English, we need to differentiate between the built-in language and the English
	translation.
	*/
	LANGCODE_SYSTEM string = "system"

	/*  The language code used when no language is explicitly assigned (yet).

	Should be used when language information is not available or cannot be
	determined. This special language code is useful when we know the data
	might have linguistic information, but we don't know the language.

	http://www.w3.org/International/questions/qa-no-language#undetermined

	*/
	LANGCODE_NOT_SPECIFIED string = "und"

	/* The language code used when the marked object has no linguistic content.

	Should be used when we explicitly know that the data referred has no linguistic content.

	http://www.w3.org/International/questions/qa-no-language#nonlinguistic
	*/
	LANGCODE_NOT_APPLICABLE string = "zxx"

	/* Language code referring to the default language of data, e.g. of an entity.

	See the BCP 47 syntax for defining private language tags:

	http://www.rfc-editor.org/rfc/bcp/bcp47.txt

	*/
	LANGCODE_DEFAULT string = "x-default"
)

// Language directions.
const (
	// Language written left to right. Possible value of language.direction.
	DIRECTION_LTR byte = iota
	// Language written right to left. Possible value of language.direction.
	DIRECTION_RTL
)

// Language states
const (
	_ byte = iota

	// The language state when referring to configurable languages.
	STATE_CONFIGURABLE // 1

	// The language state when referring to locked languages.
	STATE_LOCKED // 2

	// The language state used when referring to all languages.
	STATE_ALL // 3

	// The language state used when referring to the site's default language.
	STATE_SITE_DEFAULT // 4
)

// LanguageInterface defines the interface for language objects.
type LanguageInterface interface {
	// Name() returns the English name of the language.
	Name() string

	// GetId() gets the language ID (ISO 639-alpha2 language code).
	Id() string

	// GetDirection gets the text direction (left-to-right or right-to-left) in the language.
	//
	// Return value can only be DIRECTION_LTR or DIRECTION_RTL.
	Direction() byte

	/* GetWeight() gets the weight of the language.
	 *
	 * The weight is used to order languages with larger positive weights sinking
	 * items toward the bottom of lists.
	 */
	Weight() Weight

	// IsDefault() returns whether this language is the only site default language.
	IsDefault() bool
}

type Language struct {
	// LanguageInterface support.
	name      string
	id        string
	direction byte
	weight    Weight
	isDefault bool

	/* The language negotiation method used when a language was detected.
		 *
		 * The method ID, for example
	   * \Drupal\language\LanguageNegotiatorInterface::METHOD_ID.
	*/
	MethodId string

	/* Locked indicates a language used by the system, not an actual language.
	 *
	 * Examples of locked languages are, LANGCODE_NOT_SPECIFIED (und), and
	 * LANGCODE_NOT_APPLICABLE (zxx), which are usually shown in language selects
	 * but hidden in places like the Language configuration and cannot be deleted.
	 */
	Locked bool
}

func (l Language) Name() string {
	return l.name
}

func (l Language) Id() string {
	return l.id
}

func (l Language) Direction() byte {
	return l.direction
}

func (l Language) Weight() Weight {
	return l.weight
}

func (l Language) IsDefault() bool {
	return l.isDefault
}

// Languages is a slice of languages, extended to support sort.Interface for sorting.
type Languages []Language

// See sort.Interface
func (ll Languages) Len() int {
	return len(ll)
}

// See sort.Interface
func (ll Languages) Less(i, j int) bool {
	var ret bool

	li, lj := ll[i], ll[j]
	a_weight := li.weight
	b_weight := lj.weight

	if a_weight != b_weight {
		ret = a_weight < b_weight
	} else {
		ret = li.Name() < lj.Name()
	}

	return ret
}

// See sort.Interface
func (ll Languages) Swap(i, j int) {
	tmp := ll[i]
	ll[i] = ll[j]
	ll[j] = tmp
}
